var Ship = cc.Sprite.extend({
    ctor: function() {
        this._super();
        this.initWithFile( 'res/images/ship.png' );
        this.life = 10;
        this.isMoveUp = false;
        this.isMoveDown = false;
    },
    
    update: function(dt) {
        var y = this.getPositionY(); 
        if ( this.isMoveUp ) {
            if ( y < screenHeight - 10 ) {
                y += 10;
            }
        }
        if ( this.isMoveDown ) {
            if ( y > 10 ) {
                y -= 10;       
            }
        }
        this.setPositionY( y );
    }
    
});
