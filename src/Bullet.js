var Bullet = cc.Sprite.extend({
    ctor: function() {
        this._super();
        this.initWithFile( 'res/images/bullet.png' );
        this.speed = 5;
    },

    update: function( dt ) {
        this.move();
        this.offScreen();
    },
    
    move: function() {
        var x = this.getPositionX();
        x += this.speed;
        this.setPositionX( x );
    },
    
    offScreen: function() {
        if ( this.getPositionX() > screenWidth ) {
            this.randomPosition();
        }
    },

    randomPosition: function() {
        var y = 10 + Math.floor( Math.random() * ( screenHeight - 10 ) );
        this.setPosition( new cc.Point( 100, y ) );
    }
});

