var GameLayer = cc.LayerColor.extend({
    init: function() {
        this._super( new cc.Color( 0, 0, 0, 255 ) );
        this.setPosition( new cc.Point( 0, 0 ) );

        this.createShip();
        this.createBullet();
        this.createLifeLabel();
        
        this.addKeyboardHandlers();
        this.scheduleUpdate();
        
        return true;
    },
    
    createShip: function() {
        this.ship = new Ship();
        this.ship.setPosition( new cc.Point( 700, 300 ) );
        this.addChild( this.ship );
        this.ship.scheduleUpdate();
    },
    
    createBullet: function() {
        this.bullets = [];
        for ( var i = 0; i < 15; i++ ) {
            var p = Math.random();
            var bullet = null; 
            if ( p <= 0.4) {
                bullet = new Bullet();
            } else if ( p <= 0.7 ) {
                bullet = new FastBullet();
            } else {
                bullet = new WaveBullet();
            }
            bullet.randomPosition();
            bullet.setPositionX( 100 - 150 * i );
            this.addChild( bullet );
            bullet.scheduleUpdate();

            this.bullets.push( bullet );
        }
    },
    
    createLifeLabel: function() {
        this.lifeLabel = cc.LabelTTF.create( this.ship.life, 'Arial', 32 );
        this.lifeLabel.setPosition( cc.p( 700, 550 ) );
        this.addChild( this.lifeLabel );
    },

    update: function( dt ) {
        var self = this;
        this.bullets.forEach( function( bullet, i ) {
            var x = bullet.getPositionX();
            if ( self.isAtShip( x ) ) {
                var y = bullet.getPositionY();
                if ( Math.abs( y - self.ship.getPositionY() ) < 25 ) {
                    bullet.randomPosition();
                    self.hitBullet( self );
                    self.gameOver( self );
                    return;
                }
            }
        });
    },
    
    isAtShip: function( x ) {
        return ( x < screenWidth ) && ( x > screenWidth - 100 );
    },
    
    gameOver: function( self ) {
        if ( self.ship.life == 0 ) {
            var gameOverLabel = cc.LabelTTF.create( 'Game over', 'Arial', 60 );
            gameOverLabel.setPosition( cc.p( 400, 300 ) );
            self.addChild( gameOverLabel );
            cc.director.pause();
        }
    },
    
    hitBullet: function( self ) {
        self.ship.life -= 1;
        self.lifeLabel.setString( self.ship.life );
    },
    
    onKeyDown: function( keyCode, event ) {
        if ( keyCode == cc.KEY.up ) {
            this.ship.isMoveUp = true;
            this.ship.isMoveDown = false;
        }
        if ( keyCode == cc.KEY.down ) {
            
            this.ship.isMoveDown = true;
            this.ship.isMoveUp = false;
        }
    },
    
    onKeyUp: function( keyCode, event ) {
        if ( keyCode == cc.KEY.up ) {
            this.ship.isMoveUp = false;
            
        }
        if ( keyCode == cc.KEY.down ) {
            this.ship.isMoveDown = false;
        }
    },
    
    addKeyboardHandlers: function() {
        var self = this;
        cc.eventManager.addListener({
            event: cc.EventListener.KEYBOARD,
            onKeyPressed : function( keyCode, event ) {
                self.onKeyDown( keyCode, event );
            },
            onKeyReleased: function( keyCode, event ) {
                self.onKeyUp( keyCode, event );
            }
        }, this);
    }
});
 
var StartScene = cc.Scene.extend({
    onEnter: function() {
        this._super();
        var layer = new GameLayer();
        layer.init();
        this.addChild( layer );
    }
});
